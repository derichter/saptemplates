sap.ui.define([
    "./BaseController"
], function (BaseController) {
    "use strict";

    return BaseController.extend("saptemplates.controller.DetailObjectNotFound", {});
});
